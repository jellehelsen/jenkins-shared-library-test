/* groovylint-disable CompileStatic, NestedBlockDepth */

// mandatory arguments
if (!binding.hasVariable('repository')) {
    /* groovylint-disable-next-line ThrowException */
    throw new Exception("mandatory variable 'repository' missing")
}

// optional arguments
if (!binding.hasVariable('jobName')) {
    jobName = repository
}
if (!binding.hasVariable('description')) {
    description = ''
}
if (!binding.hasVariable('branch')) {
    branch = 'main'
}
if (!binding.hasVariable('job_parameters')) {
    out.println("[DEBUG]: no job_parameters given")
    job_parameters = { }
}
if (!binding.hasVariable('jenkinsfile')) {
    jenkinsfile = 'Jenkinsfile'
}
if (!binding.hasVariable('repoOwner')) {
    repoOwner = 'CP'
}
if (!binding.hasVariable('credentialsId')) {
    credentialsId = 'jellehelsen'
}
if (!binding.hasVariable('cronTrigger')) {
    cronTrigger = ""
}

out.println("[INFO] creating pipeline '${jobName}'")
out.println("[INFO] referencing repository '${repoOwner}/${repository}'")
out.println("[DEBUG] with parameters '${job_parameters}'")

pipelineJob(jobName) {
    description(description)
    definition {
        cpsScm {
            scm {
                git {
                    remote {
                        credentials(credentialsId)
                        url("git@gitlab.com:${repoOwner}/${repository}.git")
                    }
                    branch(branch)
                }
            }
            scriptPath(jenkinsfile)
        }
    }
}
