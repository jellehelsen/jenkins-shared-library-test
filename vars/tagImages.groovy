def call(body){
    def config = [:];
    body.resolveStrategy = Closure.DELEGATE_FIRST;
    body.delegate = config;
    body();
    def branches = [:];
    for(i=0;i<config.platforms.size();i++){
        platform = config.platforms[i];
        branches[platform] = {
            node(platform) {
                ansiColor(colorMapName: 'xterm') {
                    image = docker.image("xaggetcloud/${config.project}.${platform}:${config.version_tag}")
                    image.tag("development")
                }
            }
        }
    }
    parallel branches;
}
