def call(body){
  def config = [:];
  body.resolveStrategy = Closure.DELEGATE_FIRST;
  body.delegate = config;
  body();
  parallel {
    config.platforms.each { platform ->
      def image;
      stage(platform){
        agent {
          label platform
        }
        steps {
          ansiColor(colorMapName: 'xterm'){
            script {
              docker.withRegistry("https://index.docker.io/v1/", "docker-login"){
                image = docker.build("xaggetcloud/${config.workdir}.${platform}:${env.TAG}", "-f docker/Dockerfile.${platform} .")
                image.inside() {
                  sh "cd /${config.workdir} && npm test -- --color"
                }
              }
            }
          }
        }
      }
    }
  }
}
