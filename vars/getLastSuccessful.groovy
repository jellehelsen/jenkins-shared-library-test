#!groovy

def call(component){
  def job = Jenkins.instance.getAllItems(org.jenkinsci.plugins.workflow.job.WorkflowJob).find{job ->
    job.name.equals('feature%2Fjenkins') && job.parent?.name.equals(component) 
  }
  if(job==null){
    error "Job for component ${component} not found!"
  }
  def lastBuild = job.getLastSuccessfulBuild()
  if(lastBuild == null) {
      error "Component ${component} has no successful build available!"
  }
  def revision = lastBuild.getAction(jenkins.scm.api.SCMRevisionAction)?.revision
  return revision
}
