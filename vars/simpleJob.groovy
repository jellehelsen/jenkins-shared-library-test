void call(Map namedArgs = [:]) {
    String scriptText = libraryResource "simplejob.groovy"
    namedArgs["repoOwner"] = "hcode1"
    namedArgs["repository"] = "jenkins_test"

    jobDsl(
        scriptText: scriptText,
        removedJobAction: 'DISABLE',
        removedViewAction: 'DELETE',
        lookupStrategy: 'SEED_JOB',
        additionalParameters: namedArgs)
}
