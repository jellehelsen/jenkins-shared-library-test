def call(body){
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()
    def branches = [:]
    for(i=0;i<config.platforms.size();i++){
        platform = config.platforms[i]
        branches[platform] = {
            node(platform) {
                ansiColor(colorMapName: 'xterm') {
                    docker.image("xaggetcloud/${config.project}.${platform}:${config.version_tag}").inside() {
                        sh "cd /${config.project} && npm test -- --color"
                    }
                }
            }
        }
    }
    parallel branches
}
