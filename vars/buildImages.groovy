def call(Closure body){
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    println config

    def branches = [:]
    for(i=0;i<config.platforms.size();i++){
        platform = config.platforms[i];
        branches[platform] = {
            node(platform) {
                ansiColor(colorMapName: 'xterm') {
                    checkout scm;
                    docker.withRegistry('https://index.docker.io/v1/', 'docker-login'){
                        docker.build("xaggetcloud/${config.project}.${platform}:${config.version_tag}", "-f docker/Dockerfile.${platform} .")
                    }
                }
            }
        }
    }
    parallel branches
}
